﻿using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class LiveScore : MonoBehaviour
{
    private Text _scoreText;
    private float _elapsedTime;

    private void Start()
    {
        _scoreText = GetComponent<Text>();
    }

    private void Update()
    {
        _elapsedTime += Time.deltaTime;
    }

    private void LateUpdate()
    {
        _scoreText.text = _elapsedTime.ToString("0.00", CultureInfo.InvariantCulture);
    }
}