﻿using UnityEngine;

public class GameLogic : MonoBehaviour
{
    [SerializeField] private float _obstacleMovementSpeed;
    [SerializeField] private float _spawnerInterval;
    [SerializeField] private float _obstacleAcceleration = 0.001f;

    private float _initialObstacleMovementSpeed;
    private float _initalSpawnerInterval;

    public float ObstacleMovementSpeed
    {
        get { return _obstacleMovementSpeed; }
    }

    public float SpawnerInterval
    {
        get { return _spawnerInterval; }
    }
    
    #region Singleton

    private static GameLogic _instance;

    public static GameLogic Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            Destroy(this);
        }
    }

    #endregion

    private void Start()
    {
        _initalSpawnerInterval = _spawnerInterval;
        _initialObstacleMovementSpeed = _obstacleMovementSpeed;
    }

    private void FixedUpdate()
    {
        _obstacleMovementSpeed += _obstacleAcceleration;
        _spawnerInterval = _initalSpawnerInterval * _initialObstacleMovementSpeed / _obstacleMovementSpeed;
    }
}