﻿using UnityEngine;

public class ShapeShifter : MonoBehaviour
{
    public GameObject[] PlayerPrefabs;

    private int _currentIndex;
    //private GameObject _currentInstance;

    public void NextVisualInstance()
    {
/*
        if (_currentInstance)
        {
            Destroy(_currentInstance);
            _currentInstance = null;
        }

        _currentInstance = Instantiate(PlayerPrefabs[_currentIndex], transform);
        _currentInstance.transform.position = transform.position;
        _currentIndex = ++_currentIndex % PlayerPrefabs.Length;
*/
        PlayerPrefabs[_currentIndex].SetActive(false);
        _currentIndex = ++_currentIndex % PlayerPrefabs.Length;
        PlayerPrefabs[_currentIndex].SetActive(true);
    }
}