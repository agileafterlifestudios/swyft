﻿using UnityEngine;

public class PlayerInstanceBehaviour : MonoBehaviour
{
    private void OnDestroy()
    {
        SendMessageUpwards("Kill", SendMessageOptions.DontRequireReceiver);
    }
}