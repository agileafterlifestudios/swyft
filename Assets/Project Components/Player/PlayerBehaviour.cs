﻿using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerBehaviour : MonoBehaviour
{
    public float JumpForce;

    private Rigidbody2D _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Jump") || Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            _rigidbody.velocity = Vector3.up * JumpForce;
        }
    }

    public void Kill()
    {
        Debug.Log("GAME OVER");
        Debug.Break();
        SceneManager.LoadScene(0);
    }
}