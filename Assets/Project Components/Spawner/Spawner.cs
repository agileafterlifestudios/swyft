﻿using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject TargetPrefab;
    public float Interval;

    private float _currentInterval;

    private void Update()
    {
        _currentInterval += Time.deltaTime;

        if (_currentInterval > Interval)
        {
            _currentInterval = 0;
            SpawnObject();
        }
    }

    private void SpawnObject()
    {
        Instantiate(TargetPrefab, transform.position, Quaternion.identity);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.2f);
    }
}