﻿using System.Linq;
using UnityEngine;

public class LineSpawner : MonoBehaviour
{
    public Vector3 Range;
    public GameObject[] ObstaclePrefabs;

    private float _currentInterval;

    private void Update()
    {
        _currentInterval += Time.deltaTime;

        if (_currentInterval > GameLogic.Instance.SpawnerInterval)
        {
            _currentInterval = 0;
            SpawnObject();
        }
    }

    private void SpawnObject()
    {
        var shuffled = ObstaclePrefabs.OrderBy(x => Random.value).ToArray();

        for (int i = 0; i < shuffled.Length; i++)
        {
            var prefab = shuffled[i];
            Instantiate(prefab, transform.position + GetPoint(i), Quaternion.identity);
        }
    }

    private Vector3 GetPoint(int index)
    {
        var max = ObstaclePrefabs.Length - 1;
        Vector3 point = Range / max * index;
        return point;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(transform.position, transform.position + Range);

        Gizmos.color = Color.red;
        for (var i = 0; i < ObstaclePrefabs.Length; i++)
        {
            Gizmos.DrawSphere(transform.position + GetPoint(i), 0.2f);
        }
    }
}