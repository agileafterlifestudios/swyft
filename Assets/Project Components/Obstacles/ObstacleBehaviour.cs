﻿using UnityEngine;

public class ObstacleBehaviour : MonoBehaviour
{
    protected void Update()
    {
        transform.position += Vector3.left * GameLogic.Instance.ObstacleMovementSpeed * Time.deltaTime;
    }

    protected void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    protected  virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag(gameObject.tag))
        {
            Destroy(other.gameObject);
        }
    }
}