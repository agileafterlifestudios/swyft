﻿using UnityEngine;

public class ShifterBehaviour : ObstacleBehaviour
{
    private bool _triggerReady = true;

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (_triggerReady)
        {
            other.SendMessageUpwards("NextVisualInstance", SendMessageOptions.DontRequireReceiver);
            _triggerReady = false;
        }
    }
}